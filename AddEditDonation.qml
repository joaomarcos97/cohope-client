/***************************************************************************
**
**  Copyright (C) 2020 by Jean Lima Andrade <jeno.andrade@gmail.com>
**  Copyright (C) 2020 by Vinicius Melo <vinimelo@riseup.net>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.15
import QtQuick.Layouts 1.15

import br.edu.ifba.gsort.cohope 1.0
import "FontAwesome"

Item {
    id: addEditDonationRootItem

    property string donationKind
    property string title: ((existingDonation == undefined) ? "Cadastro":"Alteração") + " de " + ((donationKind === "O") ? "Oferta": "Demanda") + " de Doação"
    property bool dirty

    property var donation_items: []
    property int currentId: -1
    property var currentSwipe: undefined

    property var existingDonation: undefined

    ColumnLayout {
        anchors { fill: parent; margins: internal.margins }
        spacing: internal.margins

        Label {
            Layout.fillWidth: true
            horizontalAlignment: Qt.AlignHCenter
            text: "Digite os dados necessários para o cadastro da " + ((donationKind === "O") ? "oferta": "demanda") + " de doação. Você pode incluir itens clicando no botão '+' no canto inferior direito desta tela."
            wrapMode: Text.WordWrap
        }
        Label { Layout.fillWidth: true; text: "Nome" }
        TextField {
            id: donationNameTextField
            Layout.fillWidth: true
            placeholderText: "Digite o nome da doação"
            onDisplayTextChanged: dirty = true
        }
        Label { Layout.fillWidth: true; text: "Itens" }
        ListView {
            id: listView
            Layout.fillWidth: true; Layout.fillHeight: true; clip: true
            spacing: internal.margins/2
            delegate: SwipeDelegate {
                width: parent.width
                text: modelData.description + " - " + modelData.amount + " " + modelData.unity
                onClicked: { currentId = index; addItemDialog.open() }
                swipe.left: Label {
                    font.family: FontAwesome.solid; text: Icons.faTrashAlt; color: "white"
                    verticalAlignment: Label.AlignVCenter; height: parent.height; padding: 12
                    background: Rectangle { color: "tomato" }
                    MouseArea {
                        anchors.fill: parent
                        onClicked: { currentId = index; currentSwipe = swipe; delItemDialog.open() }
                    }
                }
            }
        }
    }

    Dialog {
        id: delItemDialog
        x: parent.width/2 - width/2; y: parent.height/2 - height/2;
        width: parent.width - 4*internal.margins
        title: "Remover Item"
        standardButtons: Dialog.Yes | Dialog.No
        contentItem: Label { text: "Deseja realmente excluir este item?"; wrapMode: Text.WordWrap }
        onAccepted: { donation_items.splice(currentId, 1); listView.model = donation_items; currentSwipe.close(); currentId = -1; currentSwipe = undefined; dirty = true }
        onRejected: { currentSwipe.close(); currentId = -1; currentSwipe = undefined }
    }

    Dialog {
        id: addItemDialog
        x: parent.width/2 - width/2; y: parent.height/2 - height/2;
        width: parent.width - 4*internal.margins
        title: "Adicionar Item"
        standardButtons: Dialog.Ok | Dialog.Cancel
        contentItem: ColumnLayout {
            Label { Layout.fillWidth: true; text: "Descrição" }
            TextField {
                id: textField
                Layout.fillWidth: true
                placeholderText: "Insira a descrição do item a ser adicionado"
                text: (currentId >= 0) ? donation_items[currentId].description:""
            }
            Label { Layout.fillWidth: true; text: "Unidade" }
            ComboBox {
                id: comboBox
                Layout.fillWidth: true
                model: [ {unity: "unidade(s)"}, {unity: "grama(s)"}, {unity: "pote(s) 500ml"} ]
                currentIndex: (currentId >= 0) ? indexOfValue(donation_items[currentId].unity):0
                textRole: "unity"
                font.pointSize: 14
            }
            Label { Layout.fillWidth: true; text: "Quantidade" }
            SpinBox {
                id: spinBox
                from: 1; to: 1000
                editable: true
                Layout.fillWidth: true
                value: (currentId >= 0) ? donation_items[currentId].amount:0
            }
        }
        onAccepted: {
            if (textField.text === "") {
                errorInfoDialog.message = "Por favor, informe a descrição do item!"
                addItemDialog.visible = true
                return
            }
            if (currentId === -1 ) {
                donation_items.push({ description: textField.text, unity: comboBox.currentValue, amount: spinBox.value })
            } else {
                donation_items[currentId].description = textField.text
                donation_items[currentId].unity = comboBox.currentValue
                donation_items[currentId].amount = spinBox.value
            }
            listView.model = donation_items
            dirty = true
        }
        onVisibleChanged: if (visible) textField.forceActiveFocus()
    }

    RoundButton {
        anchors { bottom: parent.bottom; right: parent.right; margins: internal.margins }
        font { family: FontAwesome.solid; styleName: "Solid"; pointSize: 14 }
        Layout.alignment: Qt.AlignBottom; padding: 22
        text: Icons.faPlus
        Material.foreground: "#FFF"; Material.background: "#1b1b1b"
        onClicked: {
            // Force binding evaluation to clear dialog fields
            currentId = -2; currentId = -1
            addItemDialog.open()
        }
    }

    Connections {
        enabled: stackView.currentItem === addEditDonationRootItem
        target: saveButton
        function onClicked() {
            if (donationNameTextField.text === "") {
                errorInfoDialog.message = "Error:Por favor, informe o título da " + ((donationKind === "O") ? "oferta!": "demanda!")
                return
            }
            if (donation_items.length < 1) {
                errorInfoDialog.message = "Error:A sua " + ((donationKind === "O") ? "oferta": "demanda") + " deve ter pelo menos um item!"
                return
            }
            var reply
            if (existingDonation == undefined)
                reply = NetworkController.post("donations", { "name": donationNameTextField.text, "kind": donationKind, "user_id": NetworkController.context.currentUser.id, "donation_items": donation_items })
            else
                reply = NetworkController.put("donations/" + existingDonation.id, { "name": donationNameTextField.text, "kind": donationKind, "user_id": NetworkController.context.currentUser.id, "donation_items": donation_items })
            reply.finished.connect((jsonObject) => {
                errorInfoDialog.message = "Info:" + ((donationKind === "O") ? "Oferta": "Demanda") + ((existingDonation == undefined) ? " cadastrada":" atualizada") + " com sucesso. Você pode visualizar todas as suas " + ((donationKind === "O") ? "ofertas": "demandas") + " de doação em '<i>Meu Perfil -> Minhas " + ((donationKind === "O") ? "ofertas": "demandas") +  " de doação</i>'!"
                if (existingDonation != undefined) {
                    stackView.pop()
                } else {
                    stackView.pop(null)
                    stackView.push("AvailableDonations.qml", { "donationKind": donationKind })
                }
            })
        }
    }

    Component.onCompleted: {
        if (existingDonation != undefined) {
            donationNameTextField.text = existingDonation.name
            donation_items = existingDonation.donation_items
            listView.model = donation_items
        }
        dirty = false
    }
}
