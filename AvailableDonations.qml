/***************************************************************************
**
**  Copyright (C) 2020 by Jean Lima Andrade <jeno.andrade@gmail.com>
**  Copyright (C) 2020 by Vinicius Melo <vinimelo@riseup.net>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.15
import QtQuick.Layouts 1.15

import br.edu.ifba.gsort.cohope 1.0
import "FontAwesome"

Item {
    property string donationKind
    property int userId: -1
    property string title: ((donationKind === "O") ? "Ofertas": "Demandas") + " de Doação"

    property int currentId: -1
    property var currentSwipe: undefined
    property var itemCommitments

    ColumnLayout {
        anchors { fill: parent; margins: internal.margins }
        spacing: internal.margins

        Label {
            Layout.fillWidth: true; horizontalAlignment: Qt.AlignHCenter
            text: (userId === -1) ? ("Abaixo você encontra as " + ((donationKind === "O") ? "ofertas": "demandas") + " de doação atualmente disponíveis. Caso já não exista uma " + ((donationKind === "O") ? "oferta": "demanda") + " compatível, você pode criar uma nova " + ((donationKind === "O") ? "demanda": "oferta") + " de doação clicando no botão '+' no canto inferior direito desta tela."):
                                    ("Abaixo você encontra as " + ((donationKind === "O") ? "ofertas": "demandas") + " de doação que você criou. Você pode criar uma nova " + ((donationKind === "O") ? "oferta": "demanda") + " de doação clicando no botão '+' no canto inferior direito desta tela.")
            wrapMode: Text.WordWrap
        }
        Label {
            Layout.fillWidth: true; horizontalAlignment: Qt.AlignHCenter
            text: ((donationKind === "O") ? "Ofertas": "Demandas") + " criadas por você aparecerão em <font color=\"teal\"><b>azul</b></font> e você pode alterá-las clicando na " +((donationKind === "O") ? "oferta": "demanda") + " e removê-las arrastando-a para a direita."
            visible: userId === -1
            wrapMode: Text.WordWrap
        }
        ListView {
            Layout.fillWidth: true; Layout.fillHeight: true
            spacing: internal.margins/2
            model: NetworkController.context.donations
            clip: true
            delegate: Frame {
                id: frameDelegate
                width: parent.width
                Material.elevation: 3
                padding: 1; topPadding: 1; bottomPadding: 1
                property int donationIndex: index
                SwipeDelegate {
                    width: parent.width
                    clip: true
                    onClicked: {
                        if (modelData.user.id === NetworkController.context.currentUser.id && modelData.donation_commitments.length == 0)
                            stackView.push("AddEditDonation.qml", { "donationKind": donationKind, "existingDonation": modelData })
                    }
                    contentItem: ColumnLayout {
                        width: parent.width
                        spacing: internal.margins
                        Label {
                            Layout.fillWidth: true
                            font { capitalization: Font.AllUppercase; bold: true }
                            horizontalAlignment: Qt.AlignHCenter
                            text: modelData.name
                            color: (modelData.user.id === NetworkController.context.currentUser.id) ? "#008080":"#1b1b1b"
                            wrapMode: Text.WordWrap
                        }
                        RowLayout {
                            Label { font { family: FontAwesome.solid; styleName: "Solid" } text: Icons.faUser; Layout.preferredWidth: 1.5*internal.margins; color: (modelData.user.id === NetworkController.context.currentUser.id) ? "#008080":"#1b1b1b" }
                            Label { text: ((donationKind === "O") ? "Oferecido por: ":"Solicitado por: ") + modelData.user.display_name; Layout.fillWidth: true; wrapMode: Text.WordWrap }
                        }
                        RowLayout {
                            Label { font { family: FontAwesome.solid; styleName: "Solid" } text: Icons.faClipboardList; Layout.preferredWidth: 1.5*internal.margins; color: (modelData.user.id === NetworkController.context.currentUser.id) ? "#008080":"#1b1b1b" }
                            Label { text: "Itens da " + ((donationKind === "O") ? "oferta:": "demanda:") }
                        }
                        Repeater {
                            id: repeater
                            model: modelData.donation_items
                            property string delegateColor: (modelData.user.id === NetworkController.context.currentUser.id) ? "#008080":"#1b1b1b"
                            Column {
                                Layout.leftMargin: 2*internal.margins
                                RowLayout {
                                    Label { font { family: FontAwesome.solid; styleName: "Solid" } text: Icons.faAngleRight; Layout.preferredWidth: 1.5*internal.margins; color: repeater.delegateColor }
                                    Label {
                                        Layout.fillWidth: true
                                        text: modelData.description + ": " + ((itemCommitments[frameDelegate.donationIndex][modelData.id] > 0) ? ("<s>" + modelData.amount + "</s> " + (modelData.amount-itemCommitments[frameDelegate.donationIndex][modelData.id])):modelData.amount)  + " " + modelData.unity
                                        wrapMode: Text.WordWrap
                                    }
                                }
                                Label { visible: itemCommitments[frameDelegate.donationIndex][modelData.id] > 0; leftPadding: 2*internal.margins; text: itemCommitments[frameDelegate.donationIndex][modelData.id] + " " + modelData.unity + " já " + ((donationKind === "O") ? "aceito(s)": "doado(s)") }
                            }
                        }
                        Button {
                            Layout.fillWidth: true
                            text: ((donationKind === "O") ? "ACEITAR DOAÇÃO":"QUERO DOAR")
                            visible: modelData.user.id !== NetworkController.context.currentUser.id
                            Material.background: Material.Teal; Material.foreground: "white"
                            onClicked: {
                                if (NetworkController.context.currentUser.email === "") {
                                    pageWaitingAuthentication = "AddDonationCommitment.qml"
                                    pageWaitingAuthenticationParams = { "existingDonation": modelData, "donationItemCommitments": itemCommitments[index] }
                                    NetworkController.login()
                                } else {
                                    stackView.push("AddDonationCommitment.qml", { "existingDonation": modelData, "donationItemCommitments": itemCommitments[index] })
                                }
                            }
                        }
                        Button {
                            Layout.fillWidth: true
                            text: "VER COMPROMISSOS"
                            visible: modelData.user.id === NetworkController.context.currentUser.id && modelData.donation_commitments.length > 0
                            Material.background: Material.Teal; Material.foreground: "white"
                            onClicked: stackView.push("DonationCommitments.qml", { "existingDonation": modelData })
                        }
                    }
                    swipe.enabled: modelData.user.id === NetworkController.context.currentUser.id
                    swipe.left: Label {
                        font.family: FontAwesome.solid; text: Icons.faTrashAlt; color: "white"
                        verticalAlignment: Label.AlignVCenter; height: parent.height; padding: 12
                        background: Rectangle { color: "tomato" }
                        MouseArea {
                            anchors.fill: parent
                            onClicked: { currentId = index; currentSwipe = swipe; delDonationDialog.open() }
                        }
                    }
                }
            }
        }
    }

    RoundButton {
        anchors { bottom: parent.bottom; right: parent.right; margins: internal.margins }
        font { family: FontAwesome.solid; styleName: "Solid"; pointSize: 14 }
        Layout.alignment: Qt.AlignBottom; padding: 22
        text: Icons.faPlus
        Material.foreground: "#FFF"; Material.background: "#1b1b1b"
        onClicked: {
            var newDonationKind = (userId === -1) ? (newDonationKind = (donationKind === "O") ? "D":"O"):donationKind
            if (NetworkController.context.currentUser.email === "") {
                pageWaitingAuthentication = "AddEditDonation.qml"
                pageWaitingAuthenticationParams = { "donationKind": newDonationKind }
                NetworkController.login()
            } else {
                stackView.push("AddEditDonation.qml", { "donationKind": newDonationKind })
            }
        }
    }

    Dialog {
        id: delDonationDialog
        x: parent.width/2 - width/2; y: parent.height/2 - height/2;
        width: parent.width - 4*internal.margins
        title: "Remover Doação"
        standardButtons: Dialog.Yes | Dialog.No
        contentItem: Label { text: "Deseja realmente excluir esta doação?"; wrapMode: Text.WordWrap }
        onAccepted: {
            NetworkController.del("donations/" + NetworkController.context.donations[currentId].id).finished.connect(
                () => { NetworkController.get("donations/findByKind?kind=" + donationKind + "&user_id=" + userId, "donations") }
            )
        }
        onRejected: { currentSwipe.close(); currentId = -1; currentSwipe = undefined }
    }
    StackView.onActivated: {
        NetworkController.get("donations/findByKind?kind=" + donationKind + "&user_id=" + userId, "donations").finished.connect(() => {
            var newItemCommitments = []
            for (var i = 0; i < NetworkController.context.donations.length; ++i) {
                var donationItems = {}
                for (var j = 0; j < NetworkController.context.donations[i].donation_items.length; ++j)
                    donationItems[NetworkController.context.donations[i].donation_items[j].id] = 0
                for (j = 0; j < NetworkController.context.donations[i].donation_commitments.length; ++j)
                    for (var k = 0; k < NetworkController.context.donations[i].donation_commitments[j].donation_commitment_items.length; ++k)
                        donationItems[NetworkController.context.donations[i].donation_commitments[j].donation_commitment_items[k].donation_item_id] += NetworkController.context.donations[i].donation_commitments[j].donation_commitment_items[k].amount
                newItemCommitments.push(donationItems)
            }
            itemCommitments = newItemCommitments
        })
    }
}
