/***************************************************************************
**
**  Copyright (C) 2020 by Jean Lima Andrade <jeno.andrade@gmail.com>
**  Copyright (C) 2020 by Vinicius Melo <vinimelo@riseup.net>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.15
import QtQuick.Layouts 1.15

import br.edu.ifba.gsort.cohope 1.0
import "FontAwesome"

Item {
    property var existingDonation
    property var donationItemCommitments
    property string title: ((existingDonation.kind === "O") ? "Compromisso de Recepção":"Compromisso de Doação")
    property var committedAmount: []

    property int currentId: -1

    ColumnLayout {
        anchors { fill: parent; margins: internal.margins }
        spacing: internal.margins
        Label {
            Layout.fillWidth: true
            font { bold: true; capitalization: Font.AllUppercase }
            text: existingDonation.name
            horizontalAlignment: Qt.AlignHCenter
            wrapMode: Text.WordWrap
        }
        RowLayout {
            Label { font { family: FontAwesome.solid; styleName: "Solid" } text: Icons.faUser; }
            Label { text: ((existingDonation.kind === "O") ? "Oferecido por: ":"Solicitado por: ") + existingDonation.user.display_name }
        }
        RowLayout {
            Label { font { family: FontAwesome.solid; styleName: "Solid" } text: Icons.faClipboardList; Layout.preferredWidth: 1.5*internal.margins; color: "#1b1b1b" }
            Label { text: "Itens da " + ((existingDonation.kind === "O") ? "oferta": "demanda:") }
        }
        ListView {
            Layout.fillWidth: true; Layout.fillHeight: true
            spacing: internal.margins/2
            model: existingDonation.donation_items
            clip: true
            delegate: Frame {
                width: parent.width
                Material.elevation: 3
                ColumnLayout {
                    width: parent.width
                    RowLayout {
                        Layout.fillWidth: true
                        Label { font { family: FontAwesome.solid; styleName: "Solid" } text: Icons.faAngleRight; Layout.preferredWidth: 1.5*internal.margins }
                        Label { Layout.fillWidth: true; text: modelData.description; wrapMode: Text.WordWrap }
                    }
                    Label { text: ((existingDonation.kind === "O") ? "Ofertado: ": "Solicitado: ") + ((donationItemCommitments[modelData.id] > 0) ? ("<s>" + modelData.amount + "</s> " + (modelData.amount-donationItemCommitments[modelData.id])):modelData.amount)  + " " + modelData.unity }
                    RowLayout {
                        Layout.fillWidth: true
                        Label { text: "Posso " + ((existingDonation.kind === "O") ? "receber:":"doar:") }
                        SpinBox {
                            id: spinBox
                            from: 0; to: modelData.amount-donationItemCommitments[modelData.id]; editable: true
                            Layout.fillWidth: true
                            onValueChanged: { committedAmount[index] = spinBox.value }
                        }
                    }
                }
            }
        }
        Button {
            id: saveButton
            text: "CONFIRMAR " + ((existingDonation.kind === "O") ? "ACEITAÇÃO":"DOAÇÃO")
            Layout.fillWidth: true
            Material.background: Material.Teal; Material.foreground: "white"
            onClicked: {
                var donationCommitmentItems = []
                for (var i = 0; i < existingDonation.donation_items.length; ++i)
                    if (committedAmount[i] > 0)
                        donationCommitmentItems.push({ "donation_item_id": existingDonation.donation_items[i].id, "amount": committedAmount[i] })
                if (donationCommitmentItems.length === 0) {
                    errorInfoDialog.message = "Error:Você precisa " + ((existingDonation.kind === "O") ? "receber": "doar") + " pelo menos uma unidade de pelo menos um item!"
                    return
                }
                donationCommitmentDialog.donationCommitmentItems = donationCommitmentItems
                donationCommitmentDialog.open()
            }
        }
    }
    Dialog {
        id: donationCommitmentDialog
        x: parent.width/2 - width/2; y: parent.height/2 - height/2;
        width: parent.width - 4*internal.margins
        title: "Confirmar " + ((existingDonation.kind === "O") ? "Recepção": "Doação")
        standardButtons: Dialog.Yes | Dialog.No
        contentItem: ColumnLayout {
            Label { Layout.fillWidth: true; text: "Deseja realmente confirmar esta " + ((existingDonation.kind === "O") ? "recepção?": "doação?"); wrapMode: Text.WordWrap }
            CheckBox { Layout.fillWidth: true; id: checkBox; text: "Manter anonimato"; checked: false; visible: (existingDonation.kind === "D") }
        }

        property var donationCommitmentItems

        onAccepted: {
            NetworkController.post("donations_commitments",
                                   { "donation_id": existingDonation.id,
                                     "committed_user_id": NetworkController.context.currentUser.id,
                                     "donation_commitment_items": donationCommitmentItems,
                                     "anonymous": checkBox.checked
                                   }
                                   ).finished.connect((jsonObject) => {
                                       errorInfoDialog.message = "Info:" + ((existingDonation.kind === "O") ? "Recepção": "Doação") + " realizada com sucesso! O usuário que criou a " + ((existingDonation.kind === "O") ? "oferta": "demanda") + " de doação será notificado sobre esta sua operação. Você pode visualizar todos os seus compromissos de doação em '<i>Meu Perfil -> Meus compromissos de doação</i>'!"
                                       stackView.pop()
                                   })
        }
    }
}
