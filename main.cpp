/***************************************************************************
**
**  Copyright (C) 2020 by Jean Lima Andrade <jeno.andrade@gmail.com>
**  Copyright (C) 2020 by Vinicius Melo <vinimelo@riseup.net>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

#include <QCoreApplication>
#include <QGuiApplication>
#include <QList>
#include <QQmlApplicationEngine>
#include <QStringLiteral>
#include <QUrl>
#include <Qt>

#include "networkcontroller.h"

#ifdef Q_OS_ANDROID
#include <QThread>
#endif

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);

    QGuiApplication::setOrganizationName(QStringLiteral("ifba"));
    QGuiApplication::setOrganizationDomain(QStringLiteral("ifba.edu.br"));
    QGuiApplication::setApplicationName(QStringLiteral("cohope"));

    QQmlApplicationEngine engine;

#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    qmlRegisterSingletonInstance<NetworkController>("br.edu.ifba.gsort.cohope",
                                                    1, 0, "NetworkController",
                                                    NetworkController::self());
#endif

#ifdef Q_OS_ANDROID
    QThread::sleep(3);
#endif

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return QGuiApplication::exec();
}
