#!/bin/bash

! [[ "$PWD" =~ cohope-client ]] && cd cohope-client
mkdir -p build && cd build && rm -rf * .* 2>/dev/null
printf "\n\nRunning clazy and link-what-you-use\n----------------------------------------\n\n\n"
export CLAZY_CHECKS="level0,level1,level2,`pacman -Ql clazy | grep manuallevel | grep README | grep -v jni | cut -d\" \" -f2 | sed -n 's/.*README\-\(.*\)\.md.*/\1/p' | paste -sd \",\" -`,no-ifndef-define-typo,no-qt6-qlatin1string-to-u"
cmake '-GNinja' -DCMAKE_CXX_COMPILER=clazy -DCMAKE_EXPORT_COMPILE_COMMANDS=ON -DCMAKE_LINK_WHAT_YOU_USE=True ../ 2>&1 >/dev/null && cmake --build .
printf "\n\nRunning clang-tidy\n----------------------------------------\n\n\n"
find ../ -name "*.h" -o -name "*.cpp" | grep -v build | xargs clang-tidy -p compile_commands.json
printf "\n\nRunning cppcheck\n------------------------------\n\n\n"
find ../ -name "*.h" -o -name "*.cpp" | grep -v build | xargs cppcheck --language=c++ --std=c++17 --inline-suppr --enable=all --suppress=preprocessorErrorDirective --suppress=unknownMacro
printf "\n\nRunning cpplint\n----------------------------------------\n\n\n"
find ../ -name "*.h" -o -name "*.cpp" | grep -v build | xargs cpplint
printf "\n\nRunning iwyu\n----------------------------------------\n\n\n"
find ../ -name "*.h" -o -name "*.cpp" | grep -v build | xargs -I {} iwyu-tool -p . {} -- -Xiwyu --mapping_file=../qt5_11.imp -Xiwyu --transitive_includes_only -Xiwyu --cxx17ns 2>/dev/null | sed -n '/should remove these lines/,/^$/p'
