package br.edu.ifba.gsort.cohope;

import org.qtproject.qt5.android.bindings.QtActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;

import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.IdpResponse;

import java.util.Arrays;
import java.util.List;

public class CohopeActivity extends QtActivity
{
    private static final String TAG = "CohopeActivity";
    private static final int RC_SIGN_IN = 123;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static native void notifyAuthentication(final String token, final String displayName, final String email, final String photoUrl);

    public void login(boolean dryRun) {
        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user == null) {
            if (!dryRun) {
                // Choose authentication providers
                List<AuthUI.IdpConfig> providers = Arrays.asList(
                        new AuthUI.IdpConfig.GoogleBuilder().build(),
                        new AuthUI.IdpConfig.FacebookBuilder().build(),
                        new AuthUI.IdpConfig.TwitterBuilder().build(),
                        new AuthUI.IdpConfig.EmailBuilder().build());

                // Create and launch sign-in intent
                startActivityForResult(
                        AuthUI.getInstance()
                                .createSignInIntentBuilder()
                                .setAvailableProviders(providers)
                                .setLogo(R.drawable.cohope)
                                .setTheme(R.style.GreenTheme)
                                .setIsSmartLockEnabled(false)
                                .build(),
                        RC_SIGN_IN);
            } else {
                notifyAuthentication("", "", "", "");
            }
        } else {
            user.getIdToken(true).addOnCompleteListener(new OnCompleteListener<GetTokenResult>() {
                    public void onComplete(@NonNull Task<GetTokenResult> task) {
                        if (task.isSuccessful()) {
                            String idToken = task.getResult().getToken();
                            // Send token to your backend via HTTPS
                            Log.d(TAG, "User already authenticated: username(" + user.getDisplayName() + "), email(" + user.getEmail() + "), photoUrl(" + user.getPhotoUrl() + "), token(" + idToken + ")");
                            notifyAuthentication(idToken,
                                                 user.getDisplayName(),
                                                 user.getEmail(),
                                                 (user.getPhotoUrl() != null) ? user.getPhotoUrl().toString():"");
                        } else {
                            // Handle error -> task.getException();
                        }
                    }
                });
        }
    }

    public void logout() {
        AuthUI.getInstance().signOut(this).addOnCompleteListener(new OnCompleteListener<Void>() {
            public void onComplete(@NonNull Task<Void> task) {
                // user is now signed out
                notifyAuthentication("", "", "", "");
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            IdpResponse response = IdpResponse.fromResultIntent(data);

            if (resultCode == RESULT_OK) {
                final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                user.getIdToken(true).addOnCompleteListener(new OnCompleteListener<GetTokenResult>() {
                    public void onComplete(@NonNull Task<GetTokenResult> task) {
                        if (task.isSuccessful()) {
                            String idToken = task.getResult().getToken();
                            // Send token to your backend via HTTPS
                            Log.d(TAG, "Firebase authentication successful: username(" + user.getDisplayName() + "), email(" + user.getEmail() + "), photoUrl(" + user.getPhotoUrl() + "), token(" + idToken + ")");
                            notifyAuthentication(idToken,
                                                 user.getDisplayName(),
                                                 user.getEmail(),
                                                 (user.getPhotoUrl() != null) ? user.getPhotoUrl().toString():"");
                        } else {
                            // Handle error -> task.getException();
                        }
                    }
                });
            } else {
                Log.d(TAG, "Firebase authentication failed with error " + resultCode);
            }
        }
    }
}
