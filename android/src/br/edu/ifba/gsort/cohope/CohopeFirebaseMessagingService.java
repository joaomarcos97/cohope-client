package br.edu.ifba.gsort.cohope;

import android.util.Log;
import com.google.firebase.messaging.FirebaseMessagingService;

public class CohopeFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "CohopeFirebaseMessagingService";

    @Override
    public void onNewToken(String token) {
        Log.d(TAG, "Refreshed token: " + token);
    }
}
