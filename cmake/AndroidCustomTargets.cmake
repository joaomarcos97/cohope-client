add_custom_target(
    signapk ${ANDROID_SDK}/build-tools/${BUILDTOOLS_VERSION}/apksigner sign
        --min-sdk-version 21
        --ks ../${KSFILE}
        --ks-pass pass:${KSPASS}
        ${CMAKE_BINARY_DIR}/android-build/build/outputs/apk/debug/android-build-debug.apk
)

add_custom_target(
    deployapk ${ANDROID_SDK}/platform-tools/adb install
        ${CMAKE_BINARY_DIR}/android-build/build/outputs/apk/debug/android-build-debug.apk
)
