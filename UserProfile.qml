/***************************************************************************
**
**  Copyright (C) 2020 by Jean Lima Andrade <jeno.andrade@gmail.com>
**  Copyright (C) 2020 by Vinicius Melo <vinimelo@riseup.net>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.15
import QtQuick.Layouts 1.15
import QtGraphicalEffects 1.15

import br.edu.ifba.gsort.cohope 1.0
import "FontAwesome"

Item {
    id: userProfileRootItem

    property string title: "Meu Perfil"
    property bool dirty

    ColumnLayout {
        anchors { fill: parent; margins: internal.margins }
        spacing: internal.margins
        Image {
            id: photoUrlImage
            Layout.alignment: Qt.AlignHCenter; Layout.preferredWidth: parent.width/4; Layout.preferredHeight: Layout.preferredWidth
            source: NetworkController.context.currentUser.photo_url
            visible: NetworkController.context.currentUser.photo_url !== ""
            layer.enabled: true
            layer.effect: OpacityMask {
                maskSource: Rectangle { width: photoUrlImage.width; height: width; radius: width/2; visible: false }
            }
        }
        Label { Layout.alignment: Qt.AlignHCenter; text: NetworkController.context.currentUser.display_name }
        Label { Layout.alignment: Qt.AlignHCenter; text: NetworkController.context.currentUser.email; Layout.topMargin: -internal.margins; Layout.bottomMargin: 2*internal.margins }
        RowLayout {
            Layout.fillWidth: true; Layout.bottomMargin: 2*internal.margins
            Frame {
                Layout.fillWidth: true
                ColumnLayout {
                    width: parent.width
                    Label { font { family: FontAwesome.solid; styleName: "Solid" } Material.foreground: Material.Teal; text: Icons.faMedal; color: "white"; Layout.alignment: Qt.AlignHCenter }
                    Label { Layout.fillWidth: true; wrapMode: Text.WordWrap; text: NetworkController.context.currentUser.amount_finished_offers + (NetworkController.context.currentUser.amount_finished_offers === 1 ? " doação finalizada":" doações finalizadas"); horizontalAlignment: Qt.AlignHCenter }
                }
                background: Rectangle { color: "#d95f02"; border.color: "#6c6c6c"; radius: 3 }
            }
            Frame {
                Layout.fillWidth: true
                ColumnLayout {
                    width: parent.width
                    Label { font { family: FontAwesome.solid; styleName: "Solid" } Material.foreground: Material.Teal; text: Icons.faTrophy; color: "white"; Layout.alignment: Qt.AlignHCenter }
                    Label { Layout.fillWidth: true; wrapMode: Text.WordWrap; text: NetworkController.context.currentUser.amount_finished_demands + (NetworkController.context.currentUser.amount_finished_demands === 1 ? " recepção finalizada":" recepções finalizadas"); horizontalAlignment: Qt.AlignHCenter }
                }
                background: Rectangle { color: "#1b9e77"; border.color: "#6c6c6c"; radius: 3 }
            }
        }
        Label { text: "Quem sou eu?" }
        TextField { id: bioTextField; Layout.fillWidth: true; text: NetworkController.context.currentUser.bio; placeholderText: "Descreva um pouco sobre você"; onDisplayTextChanged: dirty = true }
        Label { text: "Meu website:" }
        TextField { id: websiteTextField; Layout.fillWidth: true; text: NetworkController.context.currentUser.website; placeholderText: "Informe aqui o seu website"; onDisplayTextChanged: dirty = true }
        ListView {
            Layout.fillWidth: true; Layout.fillHeight: true
            spacing: internal.margins/2
            clip: true
            model: [
                { icon: Icons.faDonate, text: "Minhas ofertas de doação", page: "AvailableDonations.qml", donationKind: "O" },
                { icon: Icons.faBatteryQuarter, text: "Minhas demandas de doação", page: "AvailableDonations.qml", donationKind: "D" },
                { icon: Icons.faHandshake, text: "Meus compromissos de doação", page: "MyDonationCommitments.qml" }
            ]
            delegate: Item {
                width: parent.width; height: frame.height
                Frame {
                    id: frame
                    width: parent.width
                    Material.elevation: 3
                    RowLayout {
                        width: parent.width
                        spacing: internal.margins
                        Label {
                            font { family: FontAwesome.solid; styleName: "Solid" }
                            Material.foreground: Material.Teal
                            text: modelData.icon
                        }
                        Label {
                            Layout.fillWidth: true
                            horizontalAlignment: Qt.AlignLeft
                            font.capitalization: Font.AllUppercase
                            text: modelData.text
                        }
                        Label {
                            font { family: FontAwesome.solid; styleName: "Solid" }
                            text: Icons.faChevronRight
                        }
                    }
                }
                MouseArea {
                    anchors.fill: parent
                    onClicked: stackView.push(modelData.page, { "donationKind": modelData.donationKind, userId: NetworkController.context.currentUser.id })
                }
            }
        }
    }
    Connections {
        enabled: stackView.currentItem === userProfileRootItem
        target: saveButton
        function onClicked() {
            NetworkController.put("users/" + NetworkController.context.currentUser.id,
                                  { "email": NetworkController.context.currentUser.email,
                                    "display_name": NetworkController.context.currentUser.display_name,
                                    "photo_url": NetworkController.context.currentUser.photo_url,
                                    "website": websiteTextField.text,
                                    "bio": bioTextField.text,
                                    "amount_finished_offers": NetworkController.context.currentUser.amount_finished_offers,
                                    "amount_finished_demands": NetworkController.context.currentUser.amount_finished_demands
                                  }, "currentUser").finished.connect((jsonObject) => {
                                      errorInfoDialog.message = "Info:Perfil atualizado!"
                                      dirty = false
                                  })
        }
    }
    Component.onCompleted: dirty = false
}
