# cohope-client

Cliente mobile do Cohope - aplicativo para intermediação de doadores e receptores de materiais relacionados ao enfrentamento do COVID-19.

## Instruções de compilação

1. Instale uma boa distribuição do Linux. ;)
2. Instale o Java SDK versão 8 (no Arch, o pacote de chama jdk8-openjdk).
3. O Qt Creator irá instalar o Android SDK e NDK pra você. Ele precisará de muito espaço na partição /tmp para isso. Caso sua partição /tmp tenha menos de 6G de espaço (verifique executando o comando 'df -kh') execute os seguintes comandos para aumentar o tamanho:
   1. sudo mount -o remount,size=6G,noatime /tmp
   2. sudo rm -rf /tmp/*
4. Faça o download da versão mais recente do Qt em https://www.qt.io/download-qt-installer.
5. No instalador, indique o caminho da instalação (QT_ROOT) e selecione os seguintes componentes do grupo "Qt 5.15.0":
   1. Desktop gcc 64-bit
   2. Android
6. Execute o Qt Creator (localização: ${QTROOT}/Tools/QtCreator/bin/qtcreator).
7. Vá em Tools -> Options, selecione à esquerda o grupo "Devices" e depois a aba "Android".
8. Certifique-se que o "JDK location" foi corretamente identificado.
9. No campo "Android SDK location" informe um diretorio novo e clique no botão com as duas setinhas ("Automatically download Android SDK Tools to selected location").
10. Confirme todas as questões, confirme todas as licenças e aguarde a instalação (levará alguns minutos para concluir).
11. No campo "OpenSSL .pri location", clique no botão de download ao lado do botão "Browser".
12. Certifique-se que todas as configurações em "Android Settings" e "Android OpenSSL Settings" estão ok (com o ícone verde de 'check').
13. Feche a tela Tools -> Options e clique em File -> Open File or Project e selecione o arquivo CMakeLists.txt deste projeto.
14. Selecione os kits "Desktop Qt 5.15.0 GCC 64bit" e "Android for armeabi-v7a,arm64-v8a,x86,x86_64 (Clang Qt 5.15.0 for Android)" e clique em "Configure Project".
15. Certifique-se, no panel "General Messages", que o projeto foi configurado com sucesso.
16. No canto inferior esquerdo, acima do botão verde de 'play' certifique-se que o Kit selecionado é o do Android. Caso tenha alterado, verifique se o projeto foi configurado com sucesso.
17. Por padrão, o Qt Creator gera apks apenas com artefatos 32bit. Para criar um apk suportando tanto 32 quanto 64 bits, vá para o modo "Projects" (ícone de ferramenta do lado esquerdo ou CTRL+5) e mude de OFF para ON na opção ANDROID_BUILD_ABI_arm64-v8a. Clique em "Apply Configuration Changes".
18. Conecte o seu smartphone Android no computador (ele deve estar com 'Depuração via ADB' ativada) e pressione CTRL+R (ou clique no botão verde de 'play' no canto inferior esquerdo).
19. Confirme a solicitação de "USB debugging" na tela do seu smartphone.
20. Verifique se o seu aparelho é indicado como compatível no Qt Creator. Se necessário, clique no botão "Refresh Device List".
21. Clique em OK para compilar o projeto e implantar o aplicativo no seu celular. A primeira compilação é um pouco mais demorada que as demais porque o Gradle irá fazer o download das dependências do projeto.

## Instruções para contribuições

1. Escolha a issue/bug no qual você irá trabalhar, mova para a coluna "Doing" e clique no seu título para ver os detalhes.
2. Clique no botão "Create merge request" e troque para "Create branch".
3. Copie o nome do branch (sem alterá-lo).
4. Clique em "Create branch".
5. No seu repositório local mude para o branch criado executando: git checkout -b *nome-do-branch*.
6. Faça as modificações de código necessárias.
7. Faça git add, git commit e "git push origin *nome-do-branch*".
8. Note que o GitLab apresenta uma mensagem "To create a merge request for *nome-do-branch*, visit: ...".
9. Copie a URL informada e cole no seu browser.
10. Note como o título do Merge Request é criado a partir da mensagem do commit. Note também como o número da issue é informada na descrição. Isso faz com que a issue seja automaticamente fechada quando o Merge Request for aceito.
11. Indique o usuário do revisor no campo "Assignee".
12. No campo "Label", selecione Feature, Refactoring ou Bug.
13. Marque os checkboxes "Delete source branch when merge request is accepted" e "squash commits when merge request is accepted".
14. Observe e execute as sugestões do revisor.
15. Aguarde o revisor aprovar o Merge Request. A issue será automaticamente fechada e o branch automaticamente removido do servidor.
16. Após isso, volte para o branch master (git checkout master) e apague seu branch executando "git branch -D *nome-do-branch*".
17. Execute "git pull" no master para obter o código-fonte aceito.
18. EXecute "git fetch -p" para remover o branch remoto.
