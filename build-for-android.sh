#!/bin/bash

# Ajust the <YOUR-*> variables in qtcsettings.cmake and run this script from repository root folder

rm -rf build && mkdir build && cd build
cmake .. -C ../qtcsettings.cmake -GNinja
cmake --build . --parallel
cmake --build . --target apk --parallel
cmake --build . --target signapk --parallel
cmake --build . --target deployapk --parallel
