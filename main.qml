/***************************************************************************
**
**  Copyright (C) 2020 by Jean Lima Andrade <jeno.andrade@gmail.com>
**  Copyright (C) 2020 by Vinicius Melo <vinimelo@riseup.net>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.15
import QtQuick.Layouts 1.15
import Qt.labs.settings 1.0

import br.edu.ifba.gsort.cohope 1.0
import "FontAwesome"

ApplicationWindow {
    visible: true
    title: qsTr("Cohope")
    width: 340; height: 610

    property string pageWaitingAuthentication: ""
    property var pageWaitingAuthenticationParams: ({})
    property var currentUser: NetworkController.context.currentUser
    onCurrentUserChanged: {
        if (currentUser.display_name !== "" && pageWaitingAuthentication !== "")
            stackView.push(pageWaitingAuthentication, pageWaitingAuthenticationParams)
        pageWaitingAuthentication = ""
        Object.keys(pageWaitingAuthenticationParams).forEach(function(key) { delete pageWaitingAuthenticationParams[key]; });
    }

    header: ToolBar {
        ToolButton {
            anchors.left: parent.left
            font { family: FontAwesome.solid; styleName: "Solid" }
            text: stackView.depth > 1 ? Icons.faChevronLeft : Icons.faBars
            Material.foreground: "white"
            onClicked: stackView.depth > 1 ? stackView.pop():drawer.open()
        }
        Label {
            anchors.centerIn: parent
            text: stackView.currentItem.title
            font.bold: true
            Material.foreground: "white"
        }
        ToolButton {
            id: saveButton
            anchors.right: parent.right
            font { family: FontAwesome.solid; styleName: "Solid" }
            text: Icons.faSave
            Material.foreground: "white"
            visible: stackView.currentItem.dirty === true
        }
    }

    Drawer {
        id: drawer
        width: parent.width * 0.66
        height: parent.height
        font { family: FontAwesome.solid; styleName: "Solid" }

        ColumnLayout {
            anchors.fill: parent
            spacing: 0
            Image {
                source: "qrc:/images/cohope-drawer.png"
                Layout.preferredWidth: parent.width
                Layout.preferredHeight: (sourceSize.height/sourceSize.width)*parent.width
                fillMode: Image.PreserveAspectFit
            }
            ListView {
                Layout.fillWidth: true; Layout.fillHeight: true
                ColumnLayout {
                    width: parent.width
                    spacing: 0
                    ItemDelegate {
                        Layout.fillWidth: true
                        text: ((NetworkController.context.currentUser.email ?? "") !== "") ?
                                  Icons.faPowerOff + " " + "Logout" :
                                  Icons.faSignInAlt + " " + "Login"
                        onClicked: {
                            pageWaitingAuthentication = ""
                            Object.keys(pageWaitingAuthenticationParams).forEach(function(key) { delete pageWaitingAuthenticationParams[key]; });
                            ((NetworkController.context.currentUser.email ?? "") !== "") ?
                                       NetworkController.logout() :
                                       NetworkController.login()
                            drawer.close()
                        }
                    }
                }
            }
        }
    }

    Image {
        id: busyIndicator
        visible: NetworkController.status === NetworkController.Status.Loading
        width: parent.width * 1/3
        anchors.centerIn: parent
        source: "qrc:/images/cohope-busyindicator.png"
        antialiasing: true
        fillMode: Image.PreserveAspectFit

        SequentialAnimation on scale {
            running: busyIndicator.visible
            loops: Animation.Infinite
            NumberAnimation { from: 1; to: 1.1; duration: 500 }
            NumberAnimation { from: 1.1; to: 1; duration: 500 }
        }
        SequentialAnimation on rotation {
            running: busyIndicator.visible
            loops: Animation.Infinite
            NumberAnimation { from: 0; to: 360; duration: 1000 }
            NumberAnimation { from: 360; to: 0; duration: 1000 }
        }
    }

    QtObject {
        id: internal
        property int margins: 10
    }

    Dialog {
        id: errorInfoDialog
        property string message
        onMessageChanged: if (message !== "") errorInfoDialog.open()
        width: parent.width - 4*internal.margins
        x: parent.width/2 - width/2; y: parent.height/2 - height/2;
        title: (message.startsWith("Info")) ? "Informação":"Erro"
        standardButtons: Dialog.Ok
        contentItem: Label {
            id: messageLabel
            width: parent.width
            wrapMode: Text.WordWrap
            text: errorInfoDialog.message.replace(/^Error:/, "").replace(/^Info:/, "")
        }
        onAccepted: message = ""
        onRejected: message = ""
    }

    Connections {
        target: NetworkController
        function onErrorStringChanged() {
            errorInfoDialog.message = NetworkController.errorString
        }
    }

    StackView {
        id: stackView
        anchors.fill: parent
        initialItem: MainMenu {}
        visible: !busyIndicator.visible
    }

    Settings {
        id: settings
        property string pushNotificationDonationKind
    }

    onClosing: {
        if (Qt.platform.os == "android") {
            close.accepted = false
            if (stackView.depth > 1) stackView.pop()
        }
    }

    Component.onCompleted: NetworkController.login(true)
}
