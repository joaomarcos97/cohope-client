/***************************************************************************
**
**  Copyright (C) 2020 by Jean Lima Andrade <jeno.andrade@gmail.com>
**  Copyright (C) 2020 by Vinicius Melo <vinimelo@riseup.net>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.15
import QtQuick.Layouts 1.15

import br.edu.ifba.gsort.cohope 1.0
import "FontAwesome"

Item {
    property string title: "Meus Compromissos de Doação"

    property int currentId: -1
    property var currentSwipe: undefined

    ColumnLayout {
        anchors { fill: parent; margins: internal.margins }
        spacing: internal.margins

        Label {
            Layout.fillWidth: true; horizontalAlignment: Qt.AlignHCenter
            text: "Abaixo você encontra os seus compromissos de doação. Você pode cancelar um compromisso de doação deslizando-a para a direita."
            wrapMode: Text.WordWrap
        }
        ListView {
            Layout.fillWidth: true; Layout.fillHeight: true
            spacing: internal.margins/2
            model: NetworkController.context.donationsCommitments
            clip: true
            delegate: Frame {
                id: frameDelegate
                width: parent.width
                Material.elevation: 3
                padding: 1; topPadding: 1; bottomPadding: 1
                SwipeDelegate {
                    width: parent.width
                    clip: true
                    contentItem: ColumnLayout {
                        width: parent.width
                        spacing: internal.margins
                        Label {
                            Layout.fillWidth: true
                            font { capitalization: Font.AllUppercase; bold: true }
                            horizontalAlignment: Qt.AlignHCenter
                            text: modelData.donation.name
                            wrapMode: Text.WordWrap
                        }
                        RowLayout {
                            Label { font { family: FontAwesome.solid; styleName: "Solid" } text: Icons.faUser; Layout.preferredWidth: 1.5*internal.margins }
                            Label { text: ((modelData.donation.kind === "O") ? "Oferecido por: ":"Solicitado por: ") + modelData.donation.user.display_name; Layout.fillWidth: true; wrapMode: Text.WordWrap }
                        }
                        RowLayout {
                            visible: modelData.anonymous
                            Label { font { family: FontAwesome.solid; styleName: "Solid" } text: Icons.faUserSecret; Layout.preferredWidth: 1.5*internal.margins; color: "#008080" }
                            Label { text: "Doação anônima"; color: "#008080" }
                        }
                        Label { text: "Me comprometi em " + ((modelData.donation.kind === "O") ? "<b>receber</b>:":"<b>doar</a>:"); Layout.fillWidth: true; wrapMode: Text.WordWrap }
                        Repeater {
                            model: modelData.donation_commitment_items
                            RowLayout {
                                Layout.leftMargin: 2*internal.margins
                                Label { font { family: FontAwesome.solid; styleName: "Solid" } text: Icons.faAngleRight; Layout.preferredWidth: 1.5*internal.margins }
                                Label {
                                    Layout.fillWidth: true
                                    text: modelData.donation_item.description + ": " + modelData.amount + " de " + modelData.donation_item.amount + " " + modelData.donation_item.unity
                                    wrapMode: Text.WordWrap
                                }
                            }
                        }
                    }
                    swipe.left: Label {
                        font.family: FontAwesome.solid; text: Icons.faTrashAlt; color: "white"
                        verticalAlignment: Label.AlignVCenter; height: parent.height; padding: 12
                        background: Rectangle { color: "tomato" }
                        MouseArea {
                            anchors.fill: parent
                            onClicked: { currentId = index; currentSwipe = swipe; delDonationCommitmentDialog.open() }
                        }
                    }
                }
            }
        }
    }

    Dialog {
        id: delDonationCommitmentDialog
        x: parent.width/2 - width/2; y: parent.height/2 - height/2;
        width: parent.width - 4*internal.margins
        title: "Remover Compromisso de Doação"
        standardButtons: Dialog.Yes | Dialog.No
        contentItem: Label { text: "Deseja realmente excluir este compromisso de doação?"; wrapMode: Text.WordWrap }
        onAccepted: {
            NetworkController.del("/donations_commitments/" + NetworkController.context.donationsCommitments[currentId].id).finished.connect(
                () => {
                          errorInfoDialog.message = "Info:Compromisso removido! O usuário que criou a " + ((NetworkController.context.donationsCommitments[currentId].donation.kind === "O") ? "oferta": "demanda") + " de doação será notificado sobre esta sua operação!"
                          NetworkController.get("donations_commitments/findByUser/" + NetworkController.context.currentUser.id, "donationsCommitments")
                      }
            )
        }
        onRejected: { currentSwipe.close(); currentId = -1; currentSwipe = undefined }
    }
    StackView.onActivated: NetworkController.get("donations_commitments/findByUser/" + NetworkController.context.currentUser.id, "donationsCommitments")
}
