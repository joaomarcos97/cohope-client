/***************************************************************************
**
**  Copyright (C) 2020 by Jean Lima Andrade <jeno.andrade@gmail.com>
**  Copyright (C) 2020 by Vinicius Melo <vinimelo@riseup.net>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

#include "networkcontroller.h"

#ifdef Q_OS_ANDROID
#include <QtAndroid>
#include <QAndroidJniObject>
#endif
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QNetworkReply>
#include <QQmlEngine>
#include <QSettings>

#ifdef Q_OS_ANDROID
#include <jni.h>

#ifdef __cplusplus
extern "C" {
#endif

JNIEXPORT void JNICALL
  Java_br_edu_ifba_gsort_cohope_CohopeActivity_notifyAuthentication(JNIEnv *env,
                                                                    jobject obj,
                                                                    jstring idToken,
                                                                    jstring displayName,
                                                                    jstring email,
                                                                    jstring photoUrl)
{
    Q_UNUSED(obj)
    QString idTokenString(env->GetStringUTFChars(idToken, nullptr));
    QString displayNameString(env->GetStringUTFChars(displayName, nullptr));
    QString emailString(env->GetStringUTFChars(email, nullptr));
    QString photoUrlString(env->GetStringUTFChars(photoUrl, nullptr));
    QMetaObject::invokeMethod(NetworkController::self(),
                              "handleAuthenticationChange",
                              Qt::QueuedConnection,
                              Q_ARG(QString, idTokenString),
                              Q_ARG(QString, displayNameString),
                              Q_ARG(QString, emailString),
                              Q_ARG(QString, photoUrlString));
}

#ifdef __cplusplus
}
#endif
#endif

RESTReply::RESTReply(QNetworkReply *networkReply, QObject *parent) :
    QObject(parent)
{
    connect(networkReply, &::QNetworkReply::finished, this, [=] () {
        networkReply->deleteLater();
        auto statusCode = networkReply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
        if (statusCode != 200) {
            QString errorMessage = QJsonDocument::fromJson(networkReply->readAll()).object().
	                           value(QStringLiteral("message")).toString();
            if (!errorMessage.isEmpty())
                Q_EMIT errorStringChanged(errorMessage);
            else
                Q_EMIT errorStringChanged(
                    QStringLiteral("O servidor retornou status code %1").arg(statusCode));
            return;
        }
        QJsonDocument jsonDocument = QJsonDocument::fromJson(networkReply->readAll());
        if (jsonDocument.isObject()) {
            auto jsonObject = jsonDocument.object();
            if (jsonObject.value(QStringLiteral("error")).isString())
                Q_EMIT errorStringChanged(jsonObject.value(QStringLiteral("error")).toString());
            else
                Q_EMIT finished(QJsonValue(jsonObject));
        } else if (jsonDocument.isArray()) {
            Q_EMIT finished(QJsonValue(jsonDocument.array()));
        } else {
            Q_EMIT finished(QJsonValue{});
        }
    });
}

NetworkController *NetworkController::_self = nullptr;

NetworkController::NetworkController(QObject *parent) :
    QObject(parent),
    _context(new QQmlPropertyMap)
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    qmlRegisterAnonymousType<RESTReply>("br.edu.ifba.gsort.cohope.RESTReply", 1);
#endif
    _request.setHeader(QNetworkRequest::ContentTypeHeader,
                       QStringLiteral("application/x-www-form-urlencoded"));
#ifdef Q_OS_ANDROID
    handlePushNotificationNavigation();
 #endif
}

NetworkController *NetworkController::self()
{
    if (!_self)
        _self = new NetworkController;

    return _self;
}

NetworkController::~NetworkController()
{
    delete _context;
}

RESTReply *NetworkController::get(const QString &endpoint, const QString &resourceName)
{
    if (!resourceName.isEmpty())
        _context->clear(resourceName);
    auto *reply = request(QStringLiteral("%1/%2").arg(_RESTFULSERVERURL, endpoint));

    connect(reply, &RESTReply::finished, this, [=](const QJsonValue &jsonValue) {
        reply->deleteLater();
        if (!resourceName.isEmpty())
            _context->insert(resourceName, jsonValue);
        if (_pendingRequests == 0)
            setStatus(Status::Ready);
    });

    return reply;
}

RESTReply *NetworkController::post(const QString &endpoint,
                                   const QJsonObject &resourceData,
                                   const QString  &resourceName)
{
    if (!resourceName.isEmpty())
        _context->clear(resourceName);
    auto *reply = request(QStringLiteral("%1/%2").arg(_RESTFULSERVERURL, endpoint),
                          RequestType::Post, resourceData);

    connect(reply, &RESTReply::finished, this, [=](const QJsonValue &jsonValue) {
        reply->deleteLater();
        if (!resourceName.isEmpty())
            _context->insert(resourceName, jsonValue);
    });

    return reply;
}

RESTReply *NetworkController::put(const QString &endpoint,
                                  const QJsonObject &resourceData,
                                  const QString  &resourceName)
{
    if (!resourceName.isEmpty())
        _context->clear(resourceName);
    auto *reply = request(QStringLiteral("%1/%2").arg(_RESTFULSERVERURL, endpoint),
                          RequestType::Put, resourceData);

    connect(reply, &RESTReply::finished, this, [=](const QJsonValue &jsonValue) {
        reply->deleteLater();
        if (!resourceName.isEmpty())
            _context->insert(resourceName, jsonValue);
    });

    return reply;
}

// cppcheck-suppress unusedFunction
RESTReply *NetworkController::del(const QString &endpoint, const QString &resourceName)
{
    if (!resourceName.isEmpty())
        _context->clear(resourceName);
    auto *reply = request(QStringLiteral("%1/%2").arg(_RESTFULSERVERURL, endpoint),
                          RequestType::Delete);

    connect(reply, &RESTReply::finished, this, [=](const QJsonValue &jsonValue) {
        reply->deleteLater();
        if (!resourceName.isEmpty())
            _context->insert(resourceName, jsonValue);
    });

    return reply;
}

// cppcheck-suppress unusedFunction
QQmlPropertyMap *NetworkController::context() const
{
    return _context;
}

// cppcheck-suppress unusedFunction
NetworkController::Status NetworkController::status() const
{
    return _status;
}

QString NetworkController::errorString() const
{
    return _errorString;
}

RESTReply *NetworkController::request(const QString &url,
                                      RequestType requestType,
                                      const QJsonValue &jsonValue)
{
    _errorString.clear();
    _request.setUrl(QUrl(url));
    QNetworkReply *networkReply = nullptr;
    _pendingRequests++;
    setStatus(Status::Loading);
    if (requestType == RequestType::Get) {
        networkReply = _networkManager.get(_request);
    } else if (requestType == RequestType::Delete) {
        networkReply = _networkManager.deleteResource(_request);
    } else {
        if (!jsonValue.isObject())
            return nullptr;

        if (requestType == RequestType::Post)
            networkReply = _networkManager.post(_request,
                                                QJsonDocument(jsonValue.toObject()).toJson());
        else if (requestType == RequestType::Put)
            networkReply = _networkManager.put(_request,
                                               QJsonDocument(jsonValue.toObject()).toJson());
    }

    auto *reply = new RESTReply(networkReply);
    connect(reply, &RESTReply::finished, this, [=]() {
        _pendingRequests--;
        if (_pendingRequests == 0)
            setStatus(Status::Ready);
    });
    connect(reply, &RESTReply::errorStringChanged, this, [=](const QString &errorString) {
        setErrorString(errorString);
        _pendingRequests--;
        if (_pendingRequests == 0)
            setStatus(Status::Ready);
    });

    return reply;
}

void NetworkController::setStatus(Status status)
{
    if (_status != status) {
        _status = status;
        Q_EMIT statusChanged(status);
    }
}

void NetworkController::setErrorString(const QString &errorString)
{
    if (_errorString != errorString) {
        _errorString = errorString;
        Q_EMIT errorStringChanged(errorString);
    }
}

void NetworkController::login(bool dryRun)
{
#ifdef Q_OS_ANDROID
    QtAndroid::androidActivity().callMethod<void>("login", "(Z)V", dryRun);
#endif
}

void NetworkController::logout()
{
#ifdef Q_OS_ANDROID
    QtAndroid::androidActivity().callMethod<void>("logout", "()V");
#endif
}

void NetworkController::handleAuthenticationChange(const QString &token,
                                                   const QString &displayName,
                                                   const QString &email,
                                                   const QString &photoUrl)
{
    if (!email.isEmpty()) {
        post(QStringLiteral("users"),
             QJsonObject { { QStringLiteral("token"), token},
                           { QStringLiteral("email"), email},
                           { QStringLiteral("display_name"), displayName},
                           { QStringLiteral("photo_url"), photoUrl},
                           { QStringLiteral("website"), "" },
                           { QStringLiteral("bio"), "" } },
             QStringLiteral("currentUser"));
    } else {
        _context->insert(QStringLiteral("currentUser"), QJsonObject {{ "email", "" }});
    }
}

#ifdef Q_OS_ANDROID
void NetworkController::handlePushNotificationNavigation()
{
    QSettings settings;
    QAndroidJniObject intent =
        QtAndroid::androidActivity().callObjectMethod("getIntent", "()Landroid/content/Intent;");
    QAndroidJniObject bundle = intent.callObjectMethod("getExtras", "()Landroid/os/Bundle;");
    if (bundle.isValid()) {
        QString donationKind = bundle.callObjectMethod("get",
                          "(Ljava/lang/String;)Ljava/lang/Object;",
                          QAndroidJniObject::fromString(QStringLiteral("donation_kind")).object()).toString();
        settings.setValue("pushNotificationDonationKind", donationKind);
    }
}
#endif
